local CHANGES = [[ 
    Fix pursuit not working (interior -> exterior)
    Remove local pursuer interface, added global pursuit interface
]]

return setmetatable({
    MOD_NAME = "Pursuit",
    MOD_VERSION = 0.17,
    MIN_API = 60,
    CHANGES = CHANGES
}, {
    __tostring = function(modInfo)
        return string.format("\n[%s]\nVersion: %s\nMinimum API: %s\nChanges: %s", modInfo.MOD_NAME, modInfo.MOD_VERSION, modInfo.MIN_API, modInfo.CHANGES)
    end,
    __metatable = tostring
})

-- require("scripts.pursuit_for_omw.modInfo")
