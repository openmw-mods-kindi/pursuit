local core = require("openmw.core")
local self = require("openmw.self")

return {
    eventHandlers = {
        Pursuit_playDoorSound_eqnx = function(sound)
            core.sound.playSound3d(sound, self)
        end
    }
}