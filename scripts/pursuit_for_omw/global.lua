local async = require("openmw.async")
local util = require("openmw.util")
local core = require("openmw.core")
local types = require("openmw.types")
local aux = require("scripts.pursuit_for_omw.auxiliary")
local storage = require("openmw.storage")
local world = require("openmw.world")
local NPC_RETURN = require("openmw.interfaces").NPC_RETURN
local Pursuit_I = require("openmw.interfaces").Pursuit_eqnx

local SettingsPursuitMain = storage.globalSection("SettingsPursuitMain")

local TIME_TO_UNLOCK = 2 -- time to unlock a door

local function teleportToDoorDest(data)
    local door, actor = table.unpack(data)

    local destCellName = types.Door.destCell(door).name
    local destPos = types.Door.destPosition(door)
    local destRot = types.Door.destRotation(door)

    -- only teleport if the door is the same cell as the actor
    if actor.cell == door.cell then
        local successful, res = pcall(actor.teleport, actor, destCellName, destPos, {
            onGround = true,
            rotation = destRot
        })
        if successful then
            types.Lockable.unlock(door)
        end
    end
end

local travelToTheDoor = async:registerTimerCallback("goToTheDoor", function(data)
    local actor, target, door = data.actor, data.target, data.door
    if not (actor:isValid() and target:isValid()) then
        return
    end
    if types.Actor.isDead(target) or types.Actor.isDead(actor) then
        return
    end
    if actor.cell ~= target.cell then
        teleportToDoorDest {door, actor}
        NPC_RETURN.update_pursuingActors(actor, types.Door.destCell(door).name)
    end
end)

local function chaseCombatTarget(e)
    local actor, target, masa, canPathTarget, pathDist = table.unpack(e)
    local delay = 0
    local isVampire = false

    if not SettingsPursuitMain:get("Mod Status") then
        return
    end

    if not (actor:isValid() and target:isValid()) or types.Actor.isDead(actor) then
        return
    end

    if Pursuit_I.pursuitBlacklist[actor.recordId] or Pursuit_I.pursuitBlacklist[actor.id] then
        return
    end

    if Pursuit_I.immuneToPursuit[target.recordId] or Pursuit_I.immuneToPursuit[target.id] then
        return
    end

    local bestDoor
    local bestDoors = aux.findNearestDoorToCell(actor.position, target.cell.name, actor.cell:getAll(types.Door),
        target.position)
    for _, door in pairs(bestDoors) do
        local hasKeyToDoor = aux.actorHasKeyToDoor(actor, door)
        if not aux.isLocked(door) then
            bestDoor = door
            break
        elseif hasKeyToDoor then
            delay = delay + TIME_TO_UNLOCK
            bestDoor = door
            break
        end
    end

    if not bestDoor then
        return
    end

    local distance = (actor.position - bestDoor.position):length()
    if canPathTarget and pathDist > 0 then
        distance = pathDist -- use findPath() total distance instead
    end

    if actor.type == types.NPC then
        if aux.actorIsVampire(actor) then
            if aux.isNightTime() and aux.destinationCellIsConsideredExterior(bestDoor) then
                delay = math.huge
                isVampire = true
            end
        else
            delay = delay + distance / types.Actor.runSpeed(actor)
        end
    else -- if actor.type == types.Creature
        delay = delay + distance / aux.getCreatureRunSpeed(actor)
    end

    delay = math.max(0.1, delay - masa)

    if delay <= math.abs(SettingsPursuitMain:get("Pursue Time")) and canPathTarget then
        async:newSimulationTimer(delay, travelToTheDoor, {
            actor = actor,
            target = target,
            door = bestDoor

        })
        NPC_RETURN.returnInit(actor)
    end

    target:sendEvent("Pursuit_Debug_Pursuer_Details_eqnx", {
        actor = actor,
        target = target,
        canReachTarget = delay <= SettingsPursuitMain:get("Pursue Time"),
        canPathTarget = canPathTarget,
        delay = delay,
        distance = distance,
        isVampire = isVampire
    })

end

return {
    engineHandlers = {
        onActorActive = function(actor)
            if actor and (actor.type == types.NPC or actor.type == types.Creature) then
                actor:addScript("scripts/pursuit_for_omw/pursued.lua")
                actor:addScript("scripts/pursuit_for_omw/pursuer.lua")
            end
        end,
        onActivate = function(object, actor)
            if object.type == types.Door and types.Door.isTeleport(object) then
                if types.Door.destCell(object) == object.cell then -- a teleport door that leads to the same cell(but different coord)
                    for _, nearbyActor in pairs(world.activeActors) do
                        if nearbyActor.type ~= types.Player and types.Actor.objectIsInstance(nearbyActor) then
                            if nearbyActor ~= actor then
                                nearbyActor:sendEvent("Pursuit_goToNearestPursuitDoor_eqnx", {
                                    activatedDoor = object,
                                    activatingActor = actor
                                })
                            end
                        end
                    end
                end
            end
        end
    },
    eventHandlers = {
        -- sent from pursuer.lua / pursued.lua
        Pursuit_chaseCombatTarget_eqnx = chaseCombatTarget,

        -- sent from pursuer.lua / return.lua / return_global.lua
        Pursuit_teleportToDoorDest_eqnx = teleportToDoorDest
    }
}
