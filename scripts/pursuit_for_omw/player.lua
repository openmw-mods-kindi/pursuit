local core = require("openmw.core")
local ui = require("openmw.ui")
local self = require("openmw.self")
local storage = require("openmw.storage")
local types = require("openmw.types")
local I = require("openmw.interfaces")
local l10n = core.l10n("pursuit_for_omw")
local modInfo = require("scripts.pursuit_for_omw.modInfo")

I.Settings.registerPage {
    key = "pursuit_for_omw",
    l10n = "pursuit_for_omw",
    name = "settings_modName",
    description = l10n("settings_modDesc"):format(modInfo.MOD_VERSION)
}

local function showMessage(_, ...)
    local formatted = tostring(_):format(...)
    ui.showMessage(formatted)
    print(formatted)
end

return {
    engineHandlers = {
        onActive = function()
            local str = string.format("[%s] mod requires Lua API %s or newer!", modInfo.MOD_NAME, modInfo.MIN_API)
            assert(core.API_REVISION >= modInfo.MIN_API, str)
        end,
    },
    eventHandlers = {
        Pursuit_interfaceHelp_eqnx = function(msg)
            ui.printToConsole(msg, ui.CONSOLE_COLOR.Success)
        end,
        Pursuit_Debug_Pursuer_Details_eqnx = function(e)
            if storage.globalSection("SettingsPursuitDebug"):get("Debug") and e.target and e.target.type == types.Player then
                local far = e.canReachTarget and "Ok" or (e.isVampire and "Avoids sun" or "Too far")
                local see = e.canSeeTarget and "Ok" or "N/A" -- "No sight" wait for getLOS()
                local path = e.canPathTarget and "Ok" or "No path"
                local actorCell = e.actor.cell.name == "" and e.actor.cell.region or e.actor.cell.name
                local targetCell = e.target.cell.name == "" and e.target.cell.region or e.target.cell.name
                showMessage("\"%s from %s\" chases \"%s[%s]\"\n-----\nStatus:\nDist: %.1f units [%s]\nSight: %s\nPath: %s\nTime: %.1f sec", e.actor.recordId,
                actorCell, e.target.recordId, targetCell, e.distance, far, see, path, e.delay)
            end
        end
    }
}
