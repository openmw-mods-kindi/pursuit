local core = require("openmw.core")
local self = require("openmw.self")
local types = require("openmw.types")

local canMove = types.Actor.canMove
local pursuers = {}
local masa = 0 -- time between active and inactive state during pursuit

local function getPursued()
    if not next(pursuers) then
        pursuers = {}
        return
    end

    masa = core.getSimulationTime() - masa
    for _, pursuer in pairs(pursuers) do
        if canMove(pursuer) then
            pursuer:sendEvent("Pursuit_chaseCombatTarget_eqnx", {
                target = self,
                masa = masa
            })
        end
    end
    pursuers = {}
    masa = 0
end

return {
    engineHandlers = {
        onActive = function()
            getPursued()
        end,
        onInactive = function()
            masa = core.getSimulationTime()
        end
    },
    eventHandlers = {
        -- sent from pursuer.lua
        Pursuit_pursuerData_eqnx = function(actor)
            pursuers[actor.id] = actor
        end,
        Pursuit_getPursued_eqnx = function()
            if self.type == types.Player then
                masa = core.getSimulationTime()
            end
            getPursued()
        end,
    }
}
