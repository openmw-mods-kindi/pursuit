local storage = require("openmw.storage")
local aux_util = require("openmw_aux.util")
local SettingsPursuitMain = storage.globalSection("SettingsPursuitMain")

local immuneToPursuit = {}
local pursuitBlacklist = require("scripts.pursuit_for_omw.blacklist")

local help = [[
    info = Prints mod Info
    isActive = Whether Pursuit is currently active
    (add/remove)Immunity(obj) = Whether obj can be pursued
    (add/remove)Blacklist(obj) = Whether obj can pursue
    checkImmunity(obj) = Check whether obj is immune to pursuit
    checkBlacklist(obj) = Check whether obj can pursue
    dumpImmunity(obj) = List of objects immune to pursuit
    dumpBlacklist(obj) = List of objects that will not pursue

    e.g.
    I.Pursuit_eqnx.info
    I.Pursuit_eqnx.addBlacklist("fargoth")
    I.Pursuit_eqnx.addImmunity(world.players[1].id)
    I.Pursuit_eqnx.dumpBlacklist(world.players[1].id)
]]

return {
    interfaceName = "Pursuit_eqnx",
    interface = setmetatable({}, {
        __index = function(tbl, key)
            if key == "immuneToPursuit" then
                return immuneToPursuit
            end
            if key == "pursuitBlacklist" then
                return pursuitBlacklist
            end
            if key == "info" then
                return tostring(require("scripts.pursuit_for_omw.modInfo"))
            end
            if key == "help" then
                return help
            end
            if key == "isActive" then
                return SettingsPursuitMain:get("Mod Status")
            end
            if key == "checkImmunity" then
                return function(arg)
                    if type(arg) == "string" then
                        return immuneToPursuit[arg] or false
                    end
                    return "Argument must be a string"
                end
            end
            if key == "dumpImmunity" then
                return aux_util.deepToString(immuneToPursuit)
            end
            if key == "addImmunity" then
                return function(arg)
                    if type(arg) == "string" then
                        immuneToPursuit[arg] = true
                        return arg .. " successfully ADDED to pursuit immunity"
                    end
                    return "Argument must be a string"
                end
            end
            if key == "removeImmunity" then
                return function(arg)
                    if type(arg) == "string" then
                        immuneToPursuit[arg] = nil
                        return arg .. " successfully REMOVED from pursuit immunity"
                    end
                    return "Argument must be a string"
                end
            end
            if key == "dumpBlacklist" then
                return aux_util.deepToString(pursuitBlacklist)
            end
            if key == "checkBlacklist" then
                return function(arg)
                    if type(arg) == "string" then
                        return pursuitBlacklist[arg] or false
                    end
                    return "Argument must be a string"
                end
            end
            if key == "addBlacklist" then
                return function(arg)
                    if type(arg) == "string" then
                        pursuitBlacklist[arg] = true
                        return arg .. " successfully ADDED to pursuit blacklist"
                    end
                    return "Argument must be a string"
                end
            end
            if key == "removeBlacklist" then
                return function(arg)
                    if type(arg) == "string" then
                        pursuitBlacklist[arg] = nil
                        return arg .. " successfully REMOVED from pursuit blacklist"
                    end
                    return "Argument must be a string"
                end
            end
        end
    })
}
