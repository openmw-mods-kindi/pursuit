-- the following actors will not pursue
-- restart the game to for the changes to take effect, or enter "reloadlua" in the console
-- you can also use the given interface to add to/remove from blacklist through scripts or console
local bl = {
    ["vivec_god"] = true,
    ["yagrum bagarn"] = true,
    ["almalexia"] = true,
    ["TR_m3_Hormidac Farralie"] = true,
    ["Almalexia_warrior"] = true,
    -- ["add_your_own_actor_id_here_to_blacklist_it"] = true,
}

local function handleLazy(t, key)
    for k, v in pairs(t) do
        if k:lower() == key then
            return v
        end
    end
end
return setmetatable(bl, {
    __index = handleLazy
})
