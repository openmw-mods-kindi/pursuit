local types = require("openmw.types")
local aux_util = require("openmw_aux.util")
local core = require("openmw.core")
local time = require("openmw_aux.time")

local fAthleticsRunBonus = core.getGMST("fAthleticsRunBonus") or 1
local fBaseRunMultiplier = core.getGMST("fBaseRunMultiplier") or 1.75

local this = {}
this.include = function(moduleName)
    local status, result = pcall(require, moduleName)
    if (status) then
        return result
    end
end

this.getDaysPassed = function()
    -- same as mw global dayspassed
    return core.getGameTime() / time.day -- 86400 seconds
end

this.getGameHour = function()
    -- same as mw global gamehour
    return core.getGameTime() / time.hour % 24
end

-- remove and replace in v0.49
this.isLocked = function(door)
    if not types.Lockable then
        error("Types Lockable not found. Please update OpenMW to latest version")
        return nil
    end
    local suc, res = pcall(types.Lockable.isLocked, door)
    if suc then
        return res
    elseif door.type == types.ESM4Door and types.ESM4Door.record(door).isAutomatic then
        return false
    else
        return nil
    end
end

-- https://gitlab.com/OpenMW/openmw/-/blob/master/apps/openmw/mwclass/creature.cpp#L929
-- actual creature runSpeed = walkSpeed * (0.01 * athleticsSkill * fAthleticsRunBonus + fBaseRunMultiplier) based on wiki
this.getCreatureRunSpeed = function(actor)
    local athleticsSkill = actor.type.record(actor).combatSkill
    return types.Actor.walkSpeed(actor) * (0.01 * athleticsSkill * fAthleticsRunBonus + fBaseRunMultiplier)
end

this.actorHasKeyToDoor = function(actor, door)
    local doorKey = types.Lockable.getKeyRecord(door)
    if doorKey and types.Actor.inventory(actor):find(doorKey.id) then
        return true
    end
end

this.destinationCellIsConsideredExterior = function(door)
    return types.Door.destCell(door).isExterior or types.Door.destCell(door):hasTag("QuasiExterior")
end

this.isNightTime = function()
    return this.getGameHour() > 6 and this.getGameHour() < 20
end

this.actorIsVampire = function(actor)
    return types.Actor.activeEffects(actor):getEffect("Vampirism").magnitude > 0
end

-- targetPosition is optional
this.findNearestDoorToCell = function(pos, cell, nearbyDoors, targetPosition)
    local DOOR = types.Door
    local possibleDoors, scores = aux_util.mapFilterSort(nearbyDoors, function(door)
        if targetPosition == nil then
            targetPosition = DOOR.destPosition(door)
        end
        local totalDist = (pos - door.position):length() + (targetPosition - DOOR.destPosition(door)):length()
        return (DOOR.isTeleport(door) and DOOR.destCell(door).name == cell) and totalDist
    end)
    return possibleDoors, scores, aux_util.findMinScore(possibleDoors, function(door)
        return (pos - door.position):length()
    end)
end

return this
